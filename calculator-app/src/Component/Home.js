import React, { Component, Modal } from "react";
import { Button, Card, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../Home.css";
import photo from "../Images/photo.jpg";

let Object = [];
let i = 1;

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      num1: "",
      num2: "",
      arr: [],
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  Calculate = () => {
    let num1 = parseInt(this.state.num1);
    let num2 = parseInt(this.state.num2);
    let option = this.option.value;
    let result;

    if (option === "+") {
      result = num1 + num2;
    } else if (option === "-") {
      result = num1 - num2;
    } else if (option === "*") {
      result = num1 * num2;
    } else if (option === "/") {
      result = num1 / num2;
    } else if (option === "%") {
      result = num1 % num2;
    }
    let obj = {
      id: i++,
      finalResult: result,
    };
    Object.push(obj);
    this.setState({
      Object: [...this.state.arr, result],
    });
    console.log(result);
    console.log(Object);
  };

  render() {
    return (
      <div className="container">
        <Row className="row">
          <Col>
            <div>
              <Card style={{ width: "18rem" }}>
                <Card.Img variant="top" src={photo} />
                <Card.Body>
                  <input
                    type="text"
                    name="num1"
                    value={this.state.num1}
                    onChange={(e) => this.handleChange(e)}
                  ></input>
                  <input
                    type="text"
                    name="num2"
                    value={this.state.num2}
                    onChange={(e) => this.handleChange(e)}
                  ></input>
                  <select
                    value={this.state.value}
                    onChange={(e) => this.handleChange(e)}
                    ref={(option) => (this.option = option)}
                  >
                    <option value="+">+ Plus</option>
                    <option value="-">- Substract</option>
                    <option value="*">* Multiply</option>
                    <option value="/">/ Devide</option>
                    <option value="%">% Module</option>
                  </select>
                  <br></br>
                  <Button variant="primary" onClick={this.Calculate}>
                    Calculate
                  </Button>
                </Card.Body>
              </Card>
            </div>
          </Col>
          <Col>
            <h1>Result History</h1>
            <label className="thin">
              {Object.map((Object) => (
                <div className="resultHistory" key={Object.id}>
                  {Object.finalResult}
                </div>
              ))}
            </label>
          </Col>
        </Row>
      </div>
    );
  }
}
